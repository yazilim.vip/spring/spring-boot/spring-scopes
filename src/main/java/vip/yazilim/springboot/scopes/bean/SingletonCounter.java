package vip.yazilim.springboot.scopes.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Emre Sen - Aug 9, 2018
 */
@Component
@Scope("singleton")
public class SingletonCounter extends GlobalCounter {

}
