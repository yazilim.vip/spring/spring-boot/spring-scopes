package vip.yazilim.springboot.scopes.bean;

/**
 *
 * @author Emre Sen - Aug 9, 2018
 */
public abstract class GlobalCounter {

    private int counter = 0;

    public int increment() {
        counter++;
        return counter;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

}
